class BattleshipGame
  attr_reader :board, :player

 def initialize(player = HumanPlayer.new, board = Board.new)
   @player = player
   @board = board
 end

 def attack(pos)
   x, y = pos
   if board.grid[x][y] == :s
     board.grid[x][y] = :h
     "Hit"
   else
     board.grid[x][y] = :x
     puts "Miss"
   end
 end


 def count
   board.count
 end

  def game_over?
    @board.won?
  end

  def play_turn
    attack(player.get_play)
  end


end
