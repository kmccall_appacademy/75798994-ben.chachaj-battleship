class Board
  attr_accessor :grid

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def initialize(array = Board.default_grid)
    @grid = array
  end

  def count
    self.grid.flatten.count(:s)
  end

  def render
    puts "-----------"
    puts self.grid
    puts "-----------"

  end

  def empty?(pos = nil)
      x, y = pos
      return false if pos == nil && count > 0
      return true if pos == nil && count == 0
      grid[x][y] == nil
    end


  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    grid[row][col] = mark
  end


  def place_random_ship
    raise "Error" if full?
    pos = [rand(len), rand(len)]

    unless empty?(pos)
       pos = [rand(len), rand(len)]
    end

    grid[pos[0]][pos[1]] = :s
  end

  def len
    grid.length
  end

  def find_empty
    [rand(len), rand(len)]
  end

  def full?
    grid.flatten.length == count
  end

  def won?
    return true if grid.flatten.all? {|el| el != :s }
  end

end



if __FILE__ == $PROGRAM_NAME
  t = Board.new
  puts "datkoi"
  puts t.render
end
